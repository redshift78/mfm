{-# Language OverloadedStrings #-}

module EventHandlerNormal
  ( eventHandlerNormal
  ) where

import Brick.Main
import Brick.Types
import Brick.Util (on)
import Brick.Widgets.Core
import Brick.Widgets.Border
import Brick.BChan (writeBChan)
import Brick.Keybindings (handleKey)

import System.Exit
import System.Process
import Control.Concurrent (forkIO, threadDelay)
import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)
import Lens.Micro.Platform

import qualified Data.Text as T
import qualified Graphics.Vty as V

import Types
import MFMEvents
import MFMState
import Tab
import MFMEventHandlers
import Pane
import Jobs

eventHandlerNormal :: BrickEvent UINames MFMEvent -> MFMStateM ()
eventHandlerNormal (VtyEvent (V.EvKey k mods)) = do
  dispatcher <- use dispatcher
  clearMsg
  case (k, mods) of
    (V.KChar '\t', [])  -> mfmModifyActiveTab tabSwitchPane
    (V.KChar '.', [])   -> mfmModifyActiveTab tabToggleHidden
    (V.KChar ':', [])   -> mfmSetMode MFMPromptLine >> mfmSetMessage MessageNone
    _                   -> void $ handleKey dispatcher k mods
  where updateFS  = mfmQueueEvent EventUpdateFileSystem
        clearMsg  = mfmSetMessage MessageNone

-- Custom events
eventHandlerNormal (AppEvent event) = case event of
  EventQuit                       -> halt
  EventUpdateFileSystem           -> eventUpdateFileSystem
  EventUpdateDirectoryStatus dir  -> eventUpdateDirectoryStatus dir
  EventEditCursorFile             -> eventEditCursorFile
  EventEditFile fp                -> get >>= (\s -> suspendAndResume $ editFile s fp) >> mfmQueueEvent EventUpdateFileSystem
  EventMessage msg                -> mfmSetMessage msg
  EventShellPane                  -> eventShellPane
  EventMkDir dir                  -> eventMkDir dir
  EventRefreshCurrentTab          -> eventRefreshCurrentTab
  EventCursorUp                   -> mfmCursorUp
  EventCursorDown                 -> mfmCursorDown
  EventCursorLeft                 -> mfmCursorLeft
  EventCursorRight                -> mfmCursorRight
  EventStartup                    -> eventUpdateFileSystem >> liftIO (forkIO (threadDelay 1000000)) >> eventTick
  EventTick                       -> eventTick
  EventCopyTagged                 -> eventCopyTagged >> eventRefreshCurrentTab
  EventPaneJobs                   -> mfmSetActivePaneMode PaneModeJobs
  EventPaneFiles                  -> mfmSetActivePaneMode PaneModeFiles
  EventAddTestJob                 -> mfmAddJob testJob
  EventActionProgress jid i t     -> mfmModifyJobAction jid i $ set actionStatus $ StatusProgress t
  EventActionDone jid i           -> mfmModifyJobAction jid i $ set actionStatus StatusSuccess

eventHandlerNormal _  = return ()

