{-# Language OverloadedStrings #-}
{-# Language TemplateHaskell #-}
{-# Language LambdaCase #-}

module Jobs where

import qualified Data.Text as T
import qualified Data.Map.Strict as M

import Lens.Micro.Platform
import Control.Concurrent
import Data.Maybe (fromJust)

import Brick.BChan

import Types
import MFMEvents

-- Jobs can run concurrently
-- Every job consists of one or more actions, actions in a job run sequentially

type JobTitle = T.Text

data ActionType   = ActionTestPrompt | ActionSleep Int deriving (Eq, Show)
data ActionStatus = StatusWaiting | StatusExigent | StatusProgress T.Text | StatusCancelled | StatusFailed
                  | StatusSuccess deriving (Eq, Show)

data Action = Action
  { _actionType   :: ActionType
  , _actionStatus :: ActionStatus
  } deriving (Eq, Show)
$(makeLenses ''Action)

data Job = Job
  { _jobTitle   :: JobTitle
  , _jobActions :: [Action]
  } deriving (Eq, Show)
$(makeLenses ''Job)

{-
instance Semigroup Job where
  a <> b = Job  { _jobTitle   = a^.jobTitle <> b^.jobTitle
                , _jobActions = a^.jobActions <> b^.jobActions
                }

instance Monoid Job where
  mempty = emptyJob
-}

type JobMap     = M.Map JobID Job

emptyJob :: Job
emptyJob = Job
  { _jobTitle   = ""
  , _jobActions = mempty
  }

newJob :: JobTitle -> Job
newJob title = emptyJob & jobTitle  .~ title

-- | A new action with a status of waiting
newAction :: ActionType -> Action
newAction at = Action
  { _actionType   = at
  , _actionStatus = StatusWaiting
  }

-- | Add an action to a job's action queue
addAction :: Job -> Action -> Job
addAction j a = j & jobActions <>~ [ a ]

-- | A job used for testing
testJob :: Job
testJob = newJob "Test job"
  `addAction` newAction (ActionSleep 5)
  `addAction` newAction (ActionSleep 15)
  `addAction` newAction (ActionSleep 10)

-- | Get job status as a function of its actions' statuses
jobStatus :: Job -> ActionStatus
jobStatus job
  | all' StatusSuccess    = StatusSuccess
  | all' StatusWaiting    = StatusWaiting
  | any' StatusFailed     = StatusFailed
  | any' StatusCancelled  = StatusCancelled
  | any' StatusExigent    = StatusExigent
  | inProgress            = StatusProgress ""
  | otherwise             = StatusWaiting
  where statuses    = job^..jobActions.traverse.actionStatus
        all' s      = all (== s) statuses
        any' s      = s `elem` statuses
        inProgress  = any (\case StatusProgress _ -> True; _ -> False) statuses

-- | Repeat stepf until condition is met
-- | Run endf at end
stepUntil :: (a -> Bool)  -- ^ Condition function
          -> a            -- ^ Starting value
          -> (a -> IO a)  -- ^ Step function, must alter a to eventually reach condition
          -> (a -> IO ()) -- ^ Closing function
          -> IO ()
stepUntil condition a stepf endf
  | condition a = endf a
  | otherwise   = stepf a >>= (\a' -> stepUntil condition a' stepf endf)

doActionSleep :: BChan MFMEvent -> JobID -> Int -> Int -> IO ()
doActionSleep chan jid i d = stepUntil (== d) 0 stepf endf
  where stepf a     = progress (fromIntegral a) >> threadDelay (10^6) >> return (a + 1)
        endf a      = writeBChan chan $ EventActionDone jid i
        progress a  = writeBChan chan $ EventActionProgress jid i $ ": " <> T.pack (show (percent a)) <> " %"
        d'          = fromIntegral d
        percent a   = fromIntegral (floor (a / d' * 10000)) / 100.0

-- | Threaded job handler
jobThread :: BChan MFMEvent -> JobID -> Job -> IO ThreadId
jobThread chan jid job = forkIO $ mapM_ doAction $ zip [0..] (job^.jobActions)
  where doAction (i, action)  = case action^.actionType of
                                  ActionSleep d -> doActionSleep chan jid i d
