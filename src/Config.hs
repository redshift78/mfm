{-# Language OverloadedStrings #-}

module Config
  ( Config (..)
  , loadConfig
  ) where

import Data.ByteString (ByteString)
import Data.Maybe (maybe, fromMaybe)
import System.Environment (lookupEnv)
import Data.ByteString.Char8 (pack)

import qualified Data.Configurator as C

data Config = Config
  { configEditor  :: ByteString
  , configDummy   :: ByteString
  } deriving (Eq, Show)

defaultConfig :: Config
defaultConfig = Config
  { configEditor  = "vi"
  , configDummy   = ""
  }

-- | Returns config from environment variables, falls back to
-- | supplied config if environment variables don't exist
configFromEnvironment :: Config -> IO Config
configFromEnvironment defs = do
  editor <- lookupEnv "EDITOR"

  return Config
    { configEditor  = maybe (def configEditor) pack editor
    , configDummy   = def configDummy
    }
  where def f = f defs

-- | Returns config from file, falls back to supplied config
-- | if entries in file don't exist
configFromFile :: Config -> IO Config
configFromFile defs = do
  config <- C.load [ C.Optional "$(HOME)/.config/mfm.conf" ]

  editor <- C.lookup config "editor"

  return Config
    { configEditor  = fromMaybe (def configEditor) editor
    , configDummy   = def configDummy
    }
  where def f = f defs

-- | Loads config, config file takes preference, then environment variables,
-- | then falls back to defaults
loadConfig :: IO Config
loadConfig = configFromEnvironment defaultConfig >>= configFromFile

