{-# Language TemplateHaskell #-}

module Tab where

import Lens.Micro.Platform
import Data.List (sortOn, elemIndex)
import Data.Maybe (fromMaybe, isNothing, fromJust, isJust)
import System.Directory
import System.Posix.Files
import Brick (Result(visibilityRequests))

import qualified Data.Map.Strict as M

import Pane
import DirectoryStatus
import Types

data PaneSide = PaneLeft | PaneRight deriving (Eq, Show)

data Tab = Tab
  { _panes          :: (Pane, Pane)
  , _activePaneSide :: PaneSide
  }

$(makeLenses ''Tab)

-- | Create a new Tab starting in the current directory
defaultTab :: IO Tab
defaultTab = do
  left  <- defaultPane
  right <- defaultPane
  return $ Tab (left, right) PaneLeft

-- | helper to execute a function on the active pane
onActivePane :: (Pane -> Pane) -> Tab -> Tab
onActivePane f tab = case tab ^. activePaneSide of
  PaneLeft  -> tab & panes .~ (f left, right)
  PaneRight -> tab & panes .~ (left, f right)
  where (left, right) = tab ^. panes

fromActivePane :: (Pane -> a) -> Tab -> a
fromActivePane f tab = case tab ^. activePaneSide of
  PaneLeft  -> f left
  PaneRight -> f right
  where (left, right) = tab ^. panes

tabToggleHidden :: Tab -> Tab
tabToggleHidden = onActivePane paneToggleHidden

moveCursorDown :: DirectoryStatusMap -> Tab -> Tab
moveCursorDown = onActivePane . paneMoveCursorDown

moveCursorUp :: DirectoryStatusMap -> Tab -> Tab
moveCursorUp = onActivePane . paneMoveCursorUp

moveCursorLeft :: DirectoryStatusMap -> Tab -> Tab
moveCursorLeft _ = onActivePane paneMoveCursorLeft

moveCursorRight :: DirectoryStatusMap -> Tab -> Tab
moveCursorRight = onActivePane . paneMoveCursorRight

tabSwitchPane :: Tab -> Tab
tabSwitchPane tab
  | active == PaneLeft  = tab & activePaneSide .~ PaneRight
  | otherwise           = tab & activePaneSide .~ PaneLeft
  where active  = tab ^. activePaneSide

-- | return the active pane
tabActivePane :: Tab -> Pane
tabActivePane tab = case tab ^. activePaneSide of
  PaneLeft  -> tab ^. panes . _1
  PaneRight -> tab ^. panes . _2
