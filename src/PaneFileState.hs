{-# Language OverloadedStrings #-}
{-# Language TemplateHaskell #-}

module PaneFileState where

import Lens.Micro.Platform
import System.Directory (getCurrentDirectory, listDirectory, getHomeDirectory)
import System.Posix.Files
import System.Posix.User
import Data.List (sortOn, elemIndex)
import Data.Maybe (isNothing, fromJust, isJust)
import GHC.Data.Maybe (rightToMaybe)
import Control.Exception (try)
import Network.HostName (HostName, getHostName)
import Data.List.Extra (replace)

import qualified Data.Set as S
import qualified Data.Text as T

import Brick.Types
import Brick.AttrMap
import Brick.Widgets.Core

import Types
import GitStatus
import DirectoryStatus
import Helpers
import StateData

{-# ANN module ("HLint: ignore Use tuple-section" :: String) #-}

data PaneFileSortMode = SortDefault deriving (Eq, Show)

data PaneFileState = PaneFileState
  { _workingDirectory :: DirectoryName
  , _showHidden       :: Bool
  , _sortMode         :: PaneFileSortMode
  , _username         :: String
  , _hostname         :: HostName
  , _homedir          :: FilePath
  , _cursor           :: Maybe FilePath
  } deriving (Eq, Show)

$(makeLenses ''PaneFileState)

defaultPaneFileState :: IO PaneFileState
defaultPaneFileState = do
  currdir   <- getCurrentDirectory
  username  <- getLoginName
  hostname  <- getHostName
  homedir   <- getHomeDirectory
  return $ PaneFileState
    { _workingDirectory = currdir
    , _showHidden       = False
    , _sortMode         = SortDefault
    , _cursor           = Nothing
    , _username         = username
    , _hostname         = hostname
    , _homedir          = homedir
    }

getFileInfo :: FilePath -> FilePath -> IO DirectoryEntry
getFileInfo wd fp = do
  status <- getSymbolicLinkStatus fp'
  owner <- rightToMaybe <$> (try (getUserEntryForID $ fileOwner status) :: IO (Either IOError UserEntry))
  group <- rightToMaybe <$> (try (getGroupEntryForID $ fileGroup status) :: IO (Either IOError GroupEntry))
  return $ DirectoryEntry
    { entryRelativePath = fp
    , entryFileStatus   = status
    , entryOwnerName    = T.pack . userName <$> owner
    , entryGroupName    = T.pack . groupName <$> group
    }
  where fp' = wd <> "/" <> fp

-- | if there isn't a cursor, return a sensible one if possible
saneCursor :: DirectoryStatusMap -> PaneFileState -> Maybe FilePath
saneCursor dirmap pane
  | null entries      = Nothing
  | isNothing mcursor = Just $ entryRelativePath $ head entries
  | otherwise         = mcursor
  where mcursor = pane^.cursor
        entries = paneContents dirmap pane

paneFileStateCursorDown :: DirectoryStatusMap -> PaneFileState -> PaneFileState
paneFileStateCursorDown dirmap pane
  | null entries      = pane & cursor .~ Nothing
  | isNothing scursor = pane & cursor ?~ entryRelativePath (head entries)
  | cursorhidden      = pane & cursor ?~ entryRelativePath (head entries)
  | null following    = pane
  | otherwise         = pane & cursor ?~ next
  where entries       = paneContents dirmap pane
        scursor       = saneCursor dirmap pane
        cursorhidden  = fromJust scursor `notElem` map entryRelativePath entries
        following     = tail $ dropWhile (\fe -> entryRelativePath fe /= fromJust scursor) entries
        next          = entryRelativePath $ head following

paneFileStateCursorUp :: DirectoryStatusMap -> PaneFileState -> PaneFileState
paneFileStateCursorUp dirmap pane
  | null entries      = pane & cursor .~ Nothing
  | isNothing scursor = pane & cursor ?~ entryRelativePath (head entries)
  | null preceding    = pane
  | otherwise         = pane & cursor ?~ prev
  where entries     = paneContents dirmap pane
        scursor     = saneCursor dirmap pane
        preceding   = takeWhile (\fe -> entryRelativePath fe /= fromJust scursor) entries
        prev        = entryRelativePath $ last preceding

paneFileStateCursorLeft :: PaneFileState -> PaneFileState
paneFileStateCursorLeft pane
  | workingdir == "/" = pane
  | null workingdir'  = pane' & workingDirectory .~ "/"
  | otherwise         = pane' & workingDirectory .~ workingdir'
  where workingdir  = pane ^. workingDirectory
        workingdir' = reverse $ tail $ dropWhile (/= '/') $ reverse workingdir
        newcursor   = reverse $ takeWhile (/= '/') $ reverse workingdir
        pane'       = pane & cursor ?~ newcursor

paneFileStateCursorRight :: DirectoryStatusMap -> PaneFileState -> PaneFileState
paneFileStateCursorRight dirmap pane
  | isNothing scursor   = pane
  | isDirectory status  = (pane & workingDirectory .~ workingdir') & cursor .~ Nothing
  | otherwise           = pane
  where scursor     = saneCursor dirmap pane
        path        = fromJust scursor
        entry       = fromJust $ entryFromPath (lookupDirectoryStatus workingdir dirmap) path
        status      = entryFileStatus entry
        workingdir  = pane ^. workingDirectory
        workingdir' | workingdir == "/" = "/" <> path
                    | otherwise         = workingdir <> "/" <> path

entryFromPath :: [DirectoryEntry] -> FilePath -> Maybe DirectoryEntry
entryFromPath [] _  = Nothing
entryFromPath (e:es) path
  | entryRelativePath e == path = Just e
  | otherwise                   = entryFromPath es path

paneCursorEntry :: DirectoryStatusMap -> PaneFileState -> Maybe DirectoryEntry
paneCursorEntry dirmap pane = saneCursor dirmap pane >>= entryFromPath (lookupDirectoryStatus dir dirmap)
  where dir = pane ^. workingDirectory

paneCursorFullPath :: DirectoryStatusMap -> PaneFileState -> Maybe FullPath
paneCursorFullPath dirmap pane = (\f -> (pane ^. workingDirectory, f)) <$> saneCursor dirmap pane

-- | Sort given file entries according to sort mode
sortFiles :: PaneFileSortMode -> [DirectoryEntry] -> [DirectoryEntry]
sortFiles SortDefault fs = sortOn entryRelativePath dirsOnly ++ sortOn entryRelativePath filesOnly
  where dirsOnly    = filter (isDirectory . entryFileStatus) fs
        filesOnly   = filter (not . isDirectory . entryFileStatus) fs

-- | Helper for paneContents
paneContentsHelper :: DirectoryStatusMap -> PaneFileState -> Maybe FilePath -> [DirectoryEntry]
paneContentsHelper _ _ Nothing  = []
paneContentsHelper dirmap pane (Just fp)
  | pane ^. showHidden  = sortFiles sortmode contents
  | otherwise           = sortFiles sortmode filtered
  where contents  = lookupDirectoryStatus fp dirmap
        filtered  = filter (\fe -> (head . entryRelativePath) fe /= '.') contents
        sortmode  = pane ^. sortMode

{-
dirCursor :: Pane -> FilePath -> Maybe DirectoryEntry
dirCursor pane dir
  | null entries  = Nothing
  | showhidden    = Just cursorentry
  | onhidden      = closest left right
  | otherwise     = Just cursorentry
  where showhidden            = paneShowHidden pane
        onhidden              = head cursorfile == '.'
        entries               = paneContentsHelper pane $ Just dir
        firstentry            = head entries
        cursorentry           = fromJust $ paneCursor pane
        cursorfile            = entryRelativePath cursorentry
        contents              = paneEntries pane
        left                  = reverse $ takeWhile (\fe -> entryRelativePath fe /= cursorfile) contents
        right                 = dropWhile (\fe -> entryRelativePath fe /= cursorfile) contents
        closest [] []         = Nothing
        closest (l:ls) []     | head (entryRelativePath l) == '.'                              = closest ls []
                              | otherwise                                             = Just l
        closest [] (r:rs)     | head (entryRelativePath r) == '.'                              = closest [] rs
                              | otherwise                                             = Just r
        closest (l:ls) (r:rs) | head (entryRelativePath l) == '.' && head (entryRelativePath r) == '.'  = closest ls rs
                              | head (entryRelativePath l) == '.'                              = Just r
                              | otherwise                                             = Just l
-}

-- | Sorted visible contents for pane
paneContents :: DirectoryStatusMap -> PaneFileState -> [DirectoryEntry]
paneContents dirmap pane = paneContentsHelper dirmap pane (Just $ pane^.workingDirectory)

-- | Helper function to turn DirectoryEntry into a widget for display
-- | First argument should be true if the cursor is on this entry
widgetify :: Bool -> FilePath -> StateData -> DirectoryEntry -> Widget UINames
widgetify active dir sdata entry = visible' $ withAttr attr $ hBox [ gitstatus, tagstatus, str " ", filename, filesize ]
  where gitmap      = sdata^.gitStatusMap
        tagged      = sdata^.taggedFilePaths
        visible'    | active    = visible
                    | otherwise = id
        status      = entryFileStatus entry
        attr        | isDirectory status    = if active then attrName "attrCursorDir" else attrName "attrFileDir"
                    | isSymbolicLink status = if active then attrName "attrCursorSymlink" else attrName "attrFileSymlink"
                    | otherwise             = if active then attrName "attrCursorFile" else attrName "attrFile"
        fp          = entryRelativePath entry
        fullpath    = (dir, fp)
        tagstatus   | not (isTagged fullpath tagged)  = txt " "
                    | active && isDirectory status    = withAttr (attrName "attrTagFlagDirCursor") $ txt "+"
                    | active                          = withAttr (attrName "attrTagFlagFileCursor") $ txt "+"
                    | isDirectory status              = withAttr (attrName "attrTagFlagDir") $ txt "+"
                    | otherwise                       = withAttr (attrName "attrTagFlagFile") $ txt "+"
        mgitstatus  = gitStatusCodesForFile gitmap (dir <> "/" <> fp)
        gitstatus   = case mgitstatus of
                        Just (sc1, sc2) -> str [ sc1, sc2 ]
                        Nothing         -> str "  "
        filename    = padRight Max $ str fp
        filesize    | isDirectory status    = txt ""
                    | isSymbolicLink status = txt ""
                    | otherwise             = txt $ prettySize $ fileSize status

drawPaneFiles :: StateData -> Bool -> PaneFileState -> Widget UINames
drawPaneFiles fsdata active pane = vBox (map widgetify' contents)
  where dirmap            = fsdata^.directoryStatusMap
        contents          = paneContents dirmap pane
        mCursor           = saneCursor dirmap pane
        cursor            = fromJust mCursor
        dir               = pane ^. workingDirectory
        heading           = str (pane ^. workingDirectory)
        widgetify' entry  | active && isJust mCursor  = widgetify (cursor == entryRelativePath entry) dir fsdata entry
                          | otherwise                 = widgetify False dir fsdata entry

drawPaneFilesTitle :: Bool -> PaneFileState -> Widget UINames
drawPaneFilesTitle active pfstate = usernamew <+> at <+> hostnamew <+> str " " <+> workingdir
  where workingdir'   = replace (pfstate ^. homedir) "~" $ pfstate ^. workingDirectory
        workingdir    = withAttr attrTitle $ padRight Max $ str workingdir'
        usernamew     = withAttr attrUsername $ str $ pfstate ^. username
        hostnamew     = withAttr attrHostname $ str $ pfstate ^. hostname
        at            = withAttr attrAt $ str "@"
        attrTitle     | active    = attrName "attrPaneTitleActive"
                      | otherwise = attrName "attrPaneTitleInActive"
        attrUsername  | active    = attrName "attrPaneUsernameActive"
                      | otherwise = attrName "attrPaneUsernameInActive"
        attrHostname  | active    = attrName "attrPaneHostnameActive"
                      | otherwise = attrName "attrPaneHostnameInActive"
        attrAt        | active    = attrName "attrPaneAtActive"
                      | otherwise = attrName "attrPaneAtInActive"

