{-# Language OverloadedStrings #-}

module MFMEvents
  ( MFMEvent (..)
  , allKeyEvents
  , defaultBindings
  ) where

import Types

import qualified Data.Text as T
import qualified Graphics.Vty as V
import qualified Brick.Keybindings as K

data MFMEvent = EventUpdateFileSystem                 -- ^ Update all filesystems we know about
              | EventUpdateDirectoryStatus FilePath   -- ^ Update a specific directory's status
              | EventQuit                             -- ^ Quit the app
              | EventEditCursorFile                   -- ^ Edit the file the current cursor is on
              | EventEditFile FullPath                -- ^ Edit a specific file
              | EventMessage PromptMessage            -- ^ Set a prompt message
              | EventShellPane                        -- ^ Create a shell pane using tmux
              | EventMkDir FilePath                   -- ^ Create a directory
              | EventRefreshCurrentTab                -- ^ Refresh only the filesystems in the current tab
              | EventCursorUp
              | EventCursorDown
              | EventCursorLeft
              | EventCursorRight
              | EventTagFile                          -- ^ Toggle tag status of file or directory under cursor
              | EventTick                             -- ^ Refreshes current tab, waits 1s, then triggers itself
              | EventStartup                          -- ^ Only triggered once, at startup
              | EventCopyTagged                       -- ^ Copy tagged files to current pane's directory
              | EventPaneJobs                         -- ^ Switch active pane to displaying jobs
              | EventPaneFiles                        -- ^ Switch active pane to displaying files
              | EventAddTestJob                       -- ^ For testing purposes, add a test job to job queue
              | EventActionProgress JobID Int T.Text  -- ^ Triggered by jobThread when it starts an action
              | EventActionDone JobID Int             -- ^ Triggered by jobThread when an action is done
              deriving (Ord, Eq, Show)

allKeyEvents :: K.KeyEvents MFMEvent
allKeyEvents = K.keyEvents
  [ ("quit",          EventQuit)
  , ("refresh",       EventRefreshCurrentTab)
  , ("edit",          EventEditCursorFile)
  , ("shell",         EventShellPane)
  , ("cursor-up",     EventCursorUp)
  , ("cursor-down",   EventCursorDown)
  , ("cursor-left",   EventCursorLeft)
  , ("cursor-right",  EventCursorRight)
  , ("tag-file",      EventTagFile)
  , ("copy-tagged",   EventCopyTagged)
  ]

defaultBindings :: [ (MFMEvent, [K.Binding] ) ]
defaultBindings =
  [ (EventQuit,               [ K.bind 'q', K.bind V.KEsc ])
  , (EventRefreshCurrentTab,  [ K.bind 'r' ])
  , (EventEditCursorFile,     [ K.bind 'e' ])
  , (EventShellPane,          [ K.bind 's' ])
  , (EventCursorUp,           [ K.bind 'k', K.bind V.KUp ])
  , (EventCursorDown,         [ K.bind 'j', K.bind V.KDown ])
  , (EventCursorLeft,         [ K.bind 'h', K.bind V.KLeft ])
  , (EventCursorRight,        [ K.bind 'l', K.bind V.KRight ])
  , (EventTagFile,            [ K.bind 't', K.bind ' ' ])
  ]

