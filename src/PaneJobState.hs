{-# Language OverloadedStrings #-}
{-# Language TemplateHaskell #-}

module PaneJobState where

import Lens.Micro.Platform
import Data.Maybe (isNothing, fromJust)

import qualified Data.Text as T
import qualified Data.Map.Strict as M

import Brick.Types
import Brick.AttrMap
import Brick.Widgets.Core
import Brick.Widgets.Border

import Types
import Jobs

data PaneJobState = PaneJobState
  { _cursor :: Maybe JobID
  , _dummy  :: String
  } deriving (Eq, Show)

$(makeLenses ''PaneJobState)

defaultPaneJobState :: PaneJobState
defaultPaneJobState = PaneJobState
  { _cursor = Nothing
  , _dummy  = "TODO: Remove me"
  }

saneJobCursor :: JobMap -> PaneJobState -> Maybe JobID
saneJobCursor jobmap pjstate
  | M.null jobmap       = Nothing
  | isNothing mcursor   = Just first
  | cursor' `elem` keys = mcursor
  | otherwise           = Just first
  where mcursor = pjstate^.cursor
        cursor' = fromJust mcursor
        keys    = M.keys jobmap
        first   = head keys

{-
drawPaneJobs :: Bool -> JobMap -> PaneJobState -> Widget UINames
drawPaneJobs active jobmap pjstate = vBox
  [ section "Active Jobs" active
  , section "Waiting Jobs" waiting
  ]
  where (active, exigent, waiting, done)  = orderedIndexedJobs jobs
        mcursori                  = saneJobCursor jobs pjstate
        statattr StatusActive     = attrName "active"
        statattr StatusExigent    = attrName "exigent"
        statattr StatusWaiting    = attrName "waiting"
        statattr StatusSuccess    = attrName "success"
        statattr StatusFailed     = attrName "failed"
        statattr StatusCancelled  = attrName "cancelled"
        attr i status             = withAttr $ attrName "job" <> statattr status
                                    <> attrName (maybe "normal" (\ci -> if ci == i then "cursor" else "normal") mcursori)
        widgetify (i,j)           = attr i (jobStatus j) $ padRight Max $ txt (j ^. jobTitle)
        section t ijs             | null ijs  = emptyWidget
                                  | otherwise = vBox ([ txt t, hBorder ] <> map widgetify ijs)
-}

-- | Helper function for drawPaneJobs that turns a (JobID, Job) into a widget for display
widgetify :: Bool -> JobMap -> PaneJobState -> (JobID, Job) -> Widget UINames
widgetify active jobmap pjstate (jid, job)
  | active    = attr txtline
  | otherwise = txtline
  where txtline                     = padRight Max $ txt $ job^.jobTitle <> progressTxt
        attr                        = withAttr $ attrName "job" <> statattr status <> cursorAttr
        status                      = jobStatus job
        statattr StatusWaiting      = attrName "waiting"
        statattr (StatusProgress _) = attrName "active"
        statattr StatusSuccess      = attrName "success"
        statattr _                  = attrName "unknown"
        underCursor                 = Just jid == saneJobCursor jobmap pjstate
        inProgress                  = case status of StatusProgress _ -> True; _ -> False
        show'                       = T.pack . show
        doneCount                   = length $ filter (\a -> a^.actionStatus == StatusSuccess) (job^.jobActions)
        cursorAttr                  | underCursor = attrName "cursor"
                                    | otherwise   = attrName "normal"
        progressTxt                 | inProgress  = " (" <> show' doneCount <> " / " <> show' (length $ job^.jobActions) <> ")"
                                    | otherwise   = mempty

widgetifyExpanded :: Bool -> JobMap -> PaneJobState -> (JobID, Job) -> Widget UINames
widgetifyExpanded active jobmap pjstate (jid, job) = vBox ([ widgetify active jobmap pjstate (jid, job) ] <> actions)
  where actions                     = map actionwidget $ job^.jobActions
        progress a                  = case a^.actionStatus of
                                      StatusProgress t  -> " " <> t
                                      _                 -> mempty
        actionwidget a              = attr a $ txt $ " └ " <> show' (a^.actionType) <> progress a
        show' x                     = T.pack $ show x
        attr a                      | active    = withAttr $ attrName "job" <> statattr (a^.actionStatus) <> attrName "normal"
                                    | otherwise = withAttr $ attrName "none"
        statattr StatusWaiting      = attrName "waiting"
        statattr (StatusProgress _) = attrName "active"
        statattr StatusSuccess      = attrName "success"
        statattr _                  = attrName "unknown"

drawPaneJobs :: Bool -> JobMap -> PaneJobState -> Widget UINames
drawPaneJobs active jobmap pjstate = vBox $ map widgetify' $ M.toList jobmap
  where widgetify'  = widgetifyExpanded active jobmap pjstate

drawPaneJobsTitle :: Bool -> PaneJobState -> Widget UINames
drawPaneJobsTitle active pane = txt "Jobs"
