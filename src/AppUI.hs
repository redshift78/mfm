{-# Language OverloadedStrings #-}

module AppUI
  ( defaultApp
  ) where

import Brick.Main
import Brick.Types
import Brick.Util (on)
import Brick.Widgets.Core
import Brick.Widgets.Border
import Brick.BChan (writeBChan)
import Brick.AttrMap (attrName)

import qualified Graphics.Vty as V
import qualified Graphics.Vty.Attributes.Color as VAC
import qualified Brick.AttrMap as A

import System.Exit
import System.Process
import System.Directory
import Control.Monad.IO.Class (liftIO)
import Lens.Micro.Platform

import Types
import MFMEvents
import TabUI
import MFMState
import TabLineUI
import StatusLineUI
import Tab
import PromptLine (drawPromptLine)
import EventHandlerNormal
import EventHandlerPrompt
import StateData

defaultApp :: App MFMState MFMEvent UINames
defaultApp = App
  { appDraw         = drawUI
  , appChooseCursor = chooseCursor
  , appHandleEvent  = eventHandler
  , appStartEvent   = mfmQueueEvent EventStartup
  , appAttrMap      = attrMap
  }

drawUI :: MFMState -> [ Widget UINames ]
drawUI state = [ joinBorders $ uiTabLine state
                <=> hBorder <=> drawTab sdata tab <=> hBorder <=> uiStatusLine dirmap state <=> promptW ]
  where tab       = (state^.tabs) !! (state^.activeTab)
        mode      = state^.mfmMode
        prompt    = state^.promptLine
        promptW   = drawPromptLine (mode == MFMPromptLine) sdata prompt
        sdata     = state^.stateData
        dirmap    = sdata^.directoryStatusMap

chooseCursor :: MFMState -> [CursorLocation UINames] -> Maybe (CursorLocation UINames)
chooseCursor state locations
  | mode == MFMPromptLine = showCursorNamed PromptCursor locations
  | otherwise             = Nothing
  where mode = state^.mfmMode

eventHandler :: BrickEvent UINames MFMEvent -> EventM UINames MFMState ()
eventHandler event@(AppEvent _) = eventHandlerNormal event  -- ^ Handle app events normally, regardless of mode
eventHandler event = do
  mode <- use mfmMode
  case mode of
    MFMNormal     -> eventHandlerNormal event
    MFMPromptLine -> eventHandlerPrompt event

attrMap :: MFMState -> A.AttrMap
attrMap state = A.attrMap V.defAttr
  [ (attrName "text",  V.white `on` V.black)
  , (attrName "attrTabActive",            V.brightGreen     `on` V.black)
  , (attrName "attrTabInactive",          VAC.Color240 224  `on` V.black)
  , (attrName "attrFileSymlink",          V.brightCyan      `on` V.black)
  , (attrName "attrFileDir",              V.brightBlue      `on` V.black)
  , (attrName "attrFile",                 V.brightWhite     `on` V.black)
  , (attrName "attrCursorSymlink",        V.black           `on` V.brightCyan)
  , (attrName "attrCursorDir",            V.black           `on` V.blue)
  , (attrName "attrCursorFile",           V.black           `on` V.white)
  , (attrName "attrFilePerms",            V.brightCyan      `on` V.black)
  , (attrName "attrFileOwner",            V.brightCyan      `on` V.black)
  , (attrName "attrFileOwnerErr",         V.brightRed       `on` V.black)
  , (attrName "attrFileGroup",            V.brightCyan      `on` V.black)
  , (attrName "attrFileGroupErr",         V.brightRed       `on` V.black)
  , (attrName "attrTagFlagFile",          V.brightCyan      `on` V.black)
  , (attrName "attrTagFlagFileCursor",    V.cyan            `on` V.white)
  , (attrName "attrTagFlagDir",           V.brightCyan      `on` V.black)
  , (attrName "attrTagFlagDirCursor",     V.black           `on` V.blue)

  , (attrName "attrPaneTitleActive",      V.brightCyan      `on` V.black)
  , (attrName "attrPaneTitleInActive",    V.white           `on` V.black)
  , (attrName "attrPaneHostnameActive",   V.brightYellow    `on` V.black)
  , (attrName "attrPaneUsernameActive",   V.brightGreen     `on` V.black)
  , (attrName "attrPaneAtActive",         V.green           `on` V.black)
  , (attrName "attrPaneHostnameInActive", V.white           `on` V.black)
  , (attrName "attrPaneUsernameInActive", V.white           `on` V.black)
  , (attrName "attrPaneAtInActive",       V.white           `on` V.black)

  , (attrName "attrPromptMessageInfo",    V.white           `on` V.black)
  , (attrName "attrPromptMessageError",   V.brightRed       `on` V.black)

  -- Job Queue
  , (attrName "job" <> attrName "waiting" <> attrName "normal", V.brightWhite `on` V.black)
  , (attrName "job" <> attrName "waiting" <> attrName "cursor", V.brightWhite `on` V.blue)
  , (attrName "job" <> attrName "active" <> attrName "normal", V.brightYellow `on` V.black)
  , (attrName "job" <> attrName "active" <> attrName "cursor", V.brightYellow `on` V.blue)
  , (attrName "job" <> attrName "success" <> attrName "normal", V.brightGreen `on` V.black)
  , (attrName "job" <> attrName "success" <> attrName "cursor", V.brightGreen `on` V.blue)
  ]
