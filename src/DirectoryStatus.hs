{- |
Module			: DirectoryStatus
Description	: Status of a directory's contents
Copyright		: (c) Fred Strauss, 2023
License			: See LICENSE file

Status of entries in a directory. This is used in a map in "MFMState"
so that it is global for all "Tab"s and "Pane"s. The key to the map contains
the working directory path, so it's not included here, only relative
path entries in the directory.
-}

module DirectoryStatus
  ( DirectoryStatusKey
  , DirectoryStatusMap
  , DirectoryEntry (..)
  , DirectoryStatus (..)
  , loadDirectoryStatus
  , lookupDirectoryStatus
  ) where

import System.Posix.Files (FileStatus, getSymbolicLinkStatus, fileOwner, fileGroup)
import System.Directory (listDirectory)
import System.Posix.User (UserEntry, GroupEntry, getUserEntryForID, getGroupEntryForID, userName, groupName)
import GHC.Data.Maybe (rightToMaybe)
import Control.Exception (try)

import qualified Data.Map.Strict as M
import qualified Data.Text as T

-- | This might change if we add support for remote hosts
type DirectoryStatusKey = FilePath
type DirectoryStatus = [ DirectoryEntry ]

-- | To be used in "MFMState"
type DirectoryStatusMap = M.Map DirectoryStatusKey DirectoryStatus

data DirectoryEntry = DirectoryEntry
  { entryRelativePath :: FilePath
  , entryFileStatus   :: FileStatus
  , entryOwnerName    :: Maybe T.Text
  , entryGroupName    :: Maybe T.Text
  }

-- | getEntryDetails gets all of the file system details for a file
getEntryDetails :: DirectoryStatusKey -- ^ The directory containing the file
                -> FilePath           -- ^ The filename
                -> IO DirectoryEntry
getEntryDetails dir file = do
  status <- getSymbolicLinkStatus fp
  owner <- rightToMaybe <$> (try (getUserEntryForID $ fileOwner status) :: IO (Either IOError UserEntry))
  group <- rightToMaybe <$> (try (getGroupEntryForID $ fileGroup status) :: IO (Either IOError GroupEntry))
  return $ DirectoryEntry
    { entryRelativePath = file
    , entryFileStatus   = status
    , entryOwnerName    = T.pack . userName <$> owner
    , entryGroupName    = T.pack . groupName <$> group
    }
  where fp  = dir <> "/" <> file

-- | loadDirectoryStatus loads the status of a directory with the given key
loadDirectoryStatus :: DirectoryStatusKey -> IO (Either IOError DirectoryStatus)
loadDirectoryStatus key = do
  elist <- try (listDirectory key)
  case elist of
    Left err    -> return $ Left err
    Right list  -> Right <$> mapM (getEntryDetails key) list

lookupDirectoryStatus :: DirectoryStatusKey -> DirectoryStatusMap -> DirectoryStatus
lookupDirectoryStatus = M.findWithDefault mempty
