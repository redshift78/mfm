{-# Language OverloadedStrings #-}

module EventHandlerPrompt
  ( eventHandlerPrompt
  ) where

import Brick.Main
import Brick.Types
import Brick.Util (on)
import Brick.Widgets.Core
import Brick.Widgets.Border
import Brick.BChan (writeBChan)

import Control.Monad.IO.Class (liftIO)
import Lens.Micro.Platform

import qualified Data.Text as T
import qualified Graphics.Vty as V

import Types
import MFMEvents
import MFMState
import Tab
import PromptLine
import Pane

data PromptCommand  = CmdEmpty
                    | CmdUnknown
                    | CmdQuit
                    | CmdEdit
                    | CmdShell
                    | CmdMkDir
                    | CmdCopyTagged
                    | CmdPaneJobs
                    | CmdPaneFiles
                    | CmdAddTestJob
                    deriving (Eq, Show)

getCommand :: MFMStateM (PromptCommand, [ T.Text ])
getCommand = do
  pl <- use promptLine
  let plt     = pl^.promptText
  let words'  = T.words plt

  if T.null plt then return (CmdEmpty, []) else return (command $ head words', tail words')
  where command "quit"  = CmdQuit
        command "q"     = CmdQuit
        command "edit"  = CmdEdit
        command "e"     = CmdEdit
        command "shell" = CmdShell
        command "mkdir" = CmdMkDir
        command "copy"  = CmdCopyTagged
        command "cp"    = CmdCopyTagged
        command "jobs"  = CmdPaneJobs
        command "files" = CmdPaneFiles
        command "test"  = CmdAddTestJob
        command _       = CmdUnknown

-- | Handle a character key press
eventHandlerPrompt :: BrickEvent UINames MFMEvent -> MFMStateM ()
eventHandlerPrompt (VtyEvent (V.EvKey (V.KChar c) [])) = do
  pl    <- use promptLine
  mfmSetInput (T.snoc (pl^.promptText) c)

-- | Handle ESC
eventHandlerPrompt (VtyEvent (V.EvKey V.KEsc [])) = mfmSetInput "" >> mfmSetMode MFMNormal

-- | Handle backspace
eventHandlerPrompt (VtyEvent (V.EvKey V.KBS [])) = do
  pl <- use promptLine
  let input = pl^.promptText
  if T.null input then return () else mfmSetInput $ T.init input

-- | Handle enter
eventHandlerPrompt (VtyEvent (V.EvKey V.KEnter [])) = executePrompt

eventHandlerPrompt _  = return ()

executePrompt :: MFMStateM ()
executePrompt = do
  (cmd, args) <- getCommand
  case cmd of
    CmdEmpty      -> return ()
    CmdUnknown    -> mfmSetMessage (MessageError "Unknown command")
    CmdQuit       -> mfmQueueEvent EventQuit
    CmdEdit       -> mfmQueueEvent EventEditCursorFile
    CmdShell      -> mfmQueueEvent EventShellPane
    CmdMkDir      -> mfmQueueEvent $ EventMkDir $ T.unpack $ T.unwords args
    CmdCopyTagged -> mfmQueueEvent EventCopyTagged
    CmdPaneJobs   -> mfmQueueEvent EventPaneJobs
    CmdPaneFiles  -> mfmQueueEvent EventPaneFiles
    CmdAddTestJob -> mfmQueueEvent EventAddTestJob

  mfmSetInput ""
  mfmSetMode MFMNormal
