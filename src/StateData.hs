{-# Language TemplateHaskell #-}

module StateData where

import Lens.Micro.Platform

import qualified Data.Set as S
import qualified Data.Map.Strict as M

import DirectoryStatus
import GitStatus
import Types
import Jobs

type AvailableSpaceMap  = M.Map FilePath Integer
type TaggedFilePaths    = (DirectoryName, S.Set FileName)

data StateData = StateData
  { _directoryStatusMap :: DirectoryStatusMap
  , _gitStatusMap       :: GitStatusMap
  , _availableSpaceMap  :: AvailableSpaceMap
  , _taggedFilePaths    :: TaggedFilePaths
  , _jobs               :: JobMap
  , _nextJobID          :: JobID
  }
$(makeLenses ''StateData)

instance Semigroup StateData where
  a <> b = StateData
    { _directoryStatusMap = a^.directoryStatusMap <> b^.directoryStatusMap
    , _gitStatusMap       = a^.gitStatusMap <> b^.gitStatusMap
    , _availableSpaceMap  = a^.availableSpaceMap <> b^.availableSpaceMap
    , _taggedFilePaths    = a^.taggedFilePaths <> b^.taggedFilePaths
    , _jobs               = a^.jobs <> b^.jobs
    , _nextJobID          = max (a^.nextJobID) (b^.nextJobID)
    }

instance Monoid StateData where
  mempty = StateData mempty mempty mempty mempty mempty 0

isTagged :: FullPath -> TaggedFilePaths -> Bool
isTagged (dir, file) (tdir, tags)
  | tdir /= dir = False
  | otherwise   = file `S.member` tags

-- | Toggle fullpath in tagged files iff the directories are the same
-- | If the directories are different, clear tagged files and add this file
toggleTagFullPath :: FullPath -> TaggedFilePaths -> TaggedFilePaths
toggleTagFullPath (dir, file) (tdir, tags)
  | tdir /= dir           = (dir, S.insert file mempty)
  | file `S.member` tags  = (dir, S.delete file tags)
  | otherwise             = (dir, S.insert file tags)
