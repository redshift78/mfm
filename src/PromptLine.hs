{-# Language OverloadedStrings #-}
{-# Language TemplateHaskell #-}

module PromptLine where

import Brick.Main
import Brick.Types
import Brick.AttrMap
import Brick.Widgets.Core

import Lens.Micro.Platform

import qualified Data.Text as T
import qualified Data.Map.Strict as M

import Types
import StateData
import Jobs

data PromptLine = PromptLine
  { _promptText     :: T.Text
  , _promptMessage  :: PromptMessage
  } deriving (Eq, Show)
$(makeLenses ''PromptLine)

newPromptLine :: PromptLine
newPromptLine = PromptLine
  { _promptText    = ""
  , _promptMessage = MessageNone
  }

-- | Clear prompt input but preserve prompt message
clearPromptLine :: PromptLine -> PromptLine
clearPromptLine pl = newPromptLine & promptMessage .~ (pl^.promptMessage)

-- | Clear prompt message, preserve everything else
clearPromptMessage :: PromptLine -> PromptLine
clearPromptMessage pl = pl & promptMessage .~ MessageNone

drawPromptLine :: Bool -> StateData -> PromptLine -> Widget UINames
drawPromptLine False sdata pl = case msg of
  MessageNone     -> padLeft Max jobstxt
  MessageInfo s   -> withAttr (attrName "attrPromptMessageInfo") $ txt s
  MessageError s  -> withAttr (attrName "attrPromptMessageError") $ txt s
  where msg     = pl^.promptMessage
        numjobs = M.size $ M.filter (\j -> jobStatus j /= StatusSuccess) $ sdata^.jobs
        jobstxt | numjobs == 0  = txt " "
                | numjobs == 1  = txt "1 job"
                | otherwise     = txt $ T.pack (show numjobs) <> " jobs"

drawPromptLine True _ pl = showCursor PromptCursor (Location (cx, 0)) (txt ":" <+> txt input)
  where input = pl^.promptText
        cx    = T.length input + 1

