{-# Language OverloadedStrings #-}

module TabUI
  ( drawTab
  ) where

import Data.Maybe (isNothing, fromJust, isJust)
import System.Posix.Files
import Lens.Micro.Platform

import Brick.Main
import Brick.Types
import Brick.AttrMap
import Brick.Widgets.Core
import Brick.Widgets.Border

import Types
import MFMState
import Tab
import DirectoryStatus
import GitStatus
import Helpers
import Pane
import PaneFileState
import Jobs
import StateData

import qualified Data.Map.Strict as M

-- | Determine attribute to use for file entry
-- | If first argument is True, this file is currently selected
fileAttr :: Bool -> DirectoryEntry -> AttrName
fileAttr True fe
  | isSymbolicLink status = attrName "attrCursorSymlink"
  | isDirectory status    = attrName "attrCursorDir"
  | otherwise             = attrName "attrCursorFile"
  where status  = entryFileStatus fe

fileAttr False fe
  | isSymbolicLink status = attrName "attrFileSymlink"
  | isDirectory status    = attrName "attrFileDir"
  | otherwise             = attrName "attrFile"
  where status  = entryFileStatus fe

drawTab :: StateData -> Tab -> Widget UINames
drawTab sdata tab = hLimitPercent 50 leftW <+> vBorder <+> rightW
  where (left, right) = tab ^. panes
        activeside    = tab ^. activePaneSide
        activepane    = tabActivePane tab
        availmap      = sdata^.availableSpaceMap
        titleAttr lr  | activeside == lr  = "attrPaneTitleActive"
                      | otherwise         = "attrPaneTitleInActive"
        leftTitle     = drawPaneTitle (activeside == PaneLeft)  left
        rightTitle    = drawPaneTitle (activeside == PaneRight) right
        mLeftAvail    = M.lookup (activepane ^. fileState . workingDirectory) availmap
        mRightAvail   = M.lookup (activepane ^. fileState . workingDirectory) availmap
        leftAvail     | isNothing mLeftAvail            = txt ""
                      | left ^. mode == PaneModeFiles   = txt $ prettySize (fromJust mLeftAvail) <> " free"
                      | otherwise                       = txt ""
        rightAvail    | isNothing mRightAvail           = txt ""
                      | right ^. mode == PaneModeFiles  = txt $ prettySize (fromJust mRightAvail) <> " free"
                      | otherwise                       = txt ""
        leftContents  = viewport ViewportPaneLeft Vertical $ drawPane sdata (activeside == PaneLeft) left
        rightContents = viewport ViewportPaneRight Vertical $ drawPane sdata (activeside == PaneRight) right
        leftW         = hBox [ padRight Max leftTitle, leftAvail ] <=> hBorder <=> leftContents
        rightW        = hBox [ padRight Max rightTitle, rightAvail ] <=> hBorder <=> rightContents

