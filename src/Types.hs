module Types
  ( UINames (..)
  , PromptMessage (..)
  , FileName
  , DirectoryName
  , FullPath
  , JobID
  , fullPathToFilePath
  ) where

import qualified Data.Text as T

data UINames        = ViewportPaneLeft | ViewportPaneRight | PromptCursor deriving (Eq, Show, Ord)
data PromptMessage  = MessageNone | MessageInfo T.Text | MessageError T.Text deriving (Eq, Show, Ord)

type FileName       = FilePath                  -- ^ A filename only
type DirectoryName  = FilePath                  -- ^ A directory only, absolute path
type FullPath       = (DirectoryName, FileName) -- ^ A full absolute path to a file

type JobID    = Int

fullPathToFilePath :: FullPath -> FilePath
fullPathToFilePath (dir, file) = dir <> "/" <> file
