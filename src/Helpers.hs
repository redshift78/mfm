{-# Language OverloadedStrings #-}

module Helpers
  ( prettySize
  ) where

import qualified Data.Text as T

-- | Pretty size display for a number
-- | Used in widgetify
prettySize :: Integral a => a -> T.Text
prettySize x
  | kx < 1    = txt' x' <> " B  "
  | mx < 1    = txt' kx <> " KiB"
  | gx < 1    = txt' mx <> " MiB"
  | otherwise = txt' gx <> " GiB"
  where txt'      = T.pack . show . oneDeci
        oneDeci n = fromIntegral (floor (n * 10 + 0.5)) / 10
        x'        = fromIntegral x
        kx        = x' / 1024
        mx        = kx / 1024
        gx        = mx / 1024


