{-# Language OverloadedStrings #-}

module StatusLineUI
  ( uiStatusLine
  ) where

import Brick.Types
import Brick.AttrMap
import Brick.Widgets.Core

import Data.Maybe (isNothing, fromJust)
import System.Posix.Files
import Data.Bits
import Data.List (intersperse)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)
import Data.Time.Format
import Data.Time.LocalTime (utcToZonedTime)
import Lens.Micro.Platform

import Types
import MFMState
import Tab
import DirectoryStatus
import Pane
import PaneFileState

-- drwxr-xr-x 2 redshift redshift    4096 Feb 23 17:21 src
uiStatusLine :: DirectoryStatusMap -> MFMState -> Widget UINames
uiStatusLine dirmap state
  | panemode /= PaneModeFiles = blank
  | isNothing entry           = blank
  | otherwise                 = hBox $ intersperse (str " ")
                                [ perms
                                , str $ show $ linkCount status
                                , ownerstr
                                , groupstr
                                , str $ show $ fileSize status
                                , str timestamp
                                ]
  where blank     = txt "Mr File Manager (We just say File Manager)"
        tab       = (state^.tabs) !! (state^.activeTab)
        entry     = paneCursorEntry dirmap $ tabActivePane tab ^. fileState
        entry'    = fromJust entry
        status    = entryFileStatus entry'
        pane      = tabActivePane tab
        panemode  = _mode pane
        mode      = fileMode status
        ownerstr  | isNothing (entryOwnerName entry') = withAttr (attrName "attrFileOwnerErr") $ str $ show $ fileOwner status
                  | otherwise                         = withAttr (attrName "attrFileOwner") $ txt $ fromJust $ entryOwnerName entry'
        groupstr  | isNothing (entryGroupName entry') = withAttr (attrName "attrFileGroupErr") $ str $ show $ fileGroup status
                  | otherwise                         = withAttr (attrName "attrFileGroup") $ txt $ fromJust $ entryGroupName entry'
        modtime   = modificationTime status
        tz        = state^.timezone
        timestamp = formatTime defaultTimeLocale "%F %T" $ utcToZonedTime tz $ posixSecondsToUTCTime $ realToFrac modtime
        dflag     | isDirectory status  = 'd'
                  | otherwise           = '-'
        isSet m   | mode .&. m  == 0  = False
                  | otherwise         = True
        flag m c  | isSet m   = c
                  | otherwise = '-'
        perms     = withAttr (attrName "attrFilePerms") $ str $ dflag : flag ownerReadMode 'r' : flag ownerWriteMode 'w'
                    : flag ownerExecuteMode 'x' : flag groupReadMode 'r' : flag groupWriteMode 'w' : flag groupExecuteMode 'x'
                    : flag otherReadMode 'r' : flag otherWriteMode  'w'
                    : flag otherExecuteMode 'x' : ""
