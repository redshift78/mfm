{-# Language OverloadedStrings #-}

module MFMEventHandlers
  ( mfmEventHandlers
  , eventShellPane
  , eventEditCursorFile
  , editFile
  , eventUpdateFileSystem
  , eventUpdateDirectoryStatus
  , eventMkDir
  , eventRefreshCurrentTab
  , eventTick
  , eventCopyTagged
  ) where

import Types
import MFMState
import MFMEvents
import Tab
import PromptLine
import Pane
import PaneFileState
import DirectoryStatus
import GitStatus
import Config
import StateData

import System.Exit
import System.Process
import System.DiskSpace
import System.Posix.Files (fileExist)
import System.Posix.Directory (createDirectory)
import System.Directory (copyFileWithMetadata)
import Control.Monad.IO.Class (liftIO)
import Control.Concurrent (threadDelay, forkIO)
import Data.ByteString.Char8 (unpack)
import Lens.Micro.Platform

import qualified Data.Set as S
import qualified Data.Text as T
import qualified Data.Map.Strict as M

import Brick.Main
import Brick.Types
import Brick.Keybindings
import Brick.BChan (writeBChan)

{-# ANN module ("HLint: ignore Use tuple-section" :: String) #-}

mfmEventHandlers :: [ KeyEventHandler MFMEvent (EventM UINames MFMState) ]
mfmEventHandlers =
  [ onEvent EventShellPane        "Open a shell in a tmux pane" eventShellPane
  , onEvent EventEditCursorFile   "Edit file under cursor"      eventEditCursorFile
  , onEvent EventQuit             "Quit MFM"                    halt
  , onEvent EventUpdateFileSystem "Update filesystems"          eventUpdateFileSystem
  , onEvent EventCursorUp         "Move cursor up"              mfmCursorUp
  , onEvent EventCursorDown       "Move cursor down"            mfmCursorDown
  , onEvent EventCursorLeft       "Move cursor left"            mfmCursorLeft
  , onEvent EventCursorRight      "Move cursor right"           mfmCursorRight
  , onEvent EventTagFile          "Tag/untag file/directory"    eventTagFile
  ]

eventShellPane :: MFMStateM ()
eventShellPane = do
  tabs <- use tabs
  tabi <- use activeTab
  let workingdir = tabActivePane (tabs !! tabi) ^. fileState . workingDirectory
  (_, _, _, ph) <- liftIO $ createProcess (proc "tmux" [ "split-window" ])
    { cwd = Just workingdir
    }

  liftIO $ waitForProcess ph
  return ()

eventEditCursorFile :: MFMStateM ()
eventEditCursorFile = do
  mfp <- mfmGetActiveCursorFullPath
  case mfp of
    Just fp -> mfmQueueEvent (EventEditFile fp)
    Nothing -> return ()

editFile :: MFMState -> FullPath -> IO MFMState
editFile state (dir, file) = do
  (_, _, _, ph) <- createProcess (proc editor [ dir <> "/" <> file ])
    { delegate_ctlc = True
    }

  exitcode <- liftIO $ waitForProcess ph
  case exitcode of
    ExitFailure code  -> return $ state & promptLine . promptMessage .~ MessageError ("exit code: " <> T.pack (show code))
    ExitSuccess       -> return state

  where pl      = state^.promptLine
        editor  = unpack $ configEditor (state^.mfmConfig)

eventUpdateFileSystem :: MFMStateM ()
eventUpdateFileSystem = do
  tabs <- use tabs
  let dirs  = nub' $ concatMap (\t -> [ t^.panes._1.fileState.workingDirectory, t^.panes._2.fileState.workingDirectory ]) tabs

  -- clear git status
  mfmSetGitStatusMap mempty

  -- update each directory used in a pane
  mapM_ (mfmQueueEvent . EventUpdateDirectoryStatus) dirs
  where nub'  = S.toList . S.fromList

eventUpdateDirectoryStatus :: FilePath -> MFMStateM ()
eventUpdateDirectoryStatus dir = do
  statmap   <- zoom stateData $ use directoryStatusMap
  gitmap    <- zoom stateData $ use gitStatusMap
  availmap  <- zoom stateData $ use availableSpaceMap

  -- get available space
  avail <- liftIO $ getAvailSpace dir
  mfmSetAvailableSpaceMap $ M.insert dir avail availmap

  -- get files in dir
  edirstatus <- liftIO $ loadDirectoryStatus dir
  case edirstatus of
    Left err        -> mfmQueueEvent $ EventMessage $ MessageError $ T.pack (show err)
    Right dirstatus -> mfmSetDirectoryStatusMap $  M.insert dir dirstatus statmap

  -- get git status
  gitmap' <- liftIO $ loadGitStatus gitmap dir
  mfmSetGitStatusMap gitmap'

eventMkDir :: FilePath -> MFMStateM ()
eventMkDir dir = do
  wdir <- mfmGetActiveWorkingDirectory
  liftIO (createDirectory (wdir <> "/" <> dir) 0o0777)
  mfmQueueEvent EventRefreshCurrentTab

  -- set cursor to new dir
  mfmOnActivePane (\p -> p&fileState.cursor ?~ dir)

eventRefreshCurrentTab :: MFMStateM ()
eventRefreshCurrentTab = do
  tabs <- use tabs
  tabi <- use activeTab
  let (left, right) = (tabs !! tabi) ^. panes
  let dirs = nub' [ left ^. fileState . workingDirectory, right ^. fileState . workingDirectory ]

  -- clear git status
  mfmSetGitStatusMap mempty

  mapM_ (mfmQueueEvent . EventUpdateDirectoryStatus) dirs
  where nub'  = S.toList . S.fromList

eventTagFile :: MFMStateM ()
eventTagFile = do
  tagged  <- zoom stateData $ use taggedFilePaths
  mfp     <- mfmGetActiveCursorFullPath
  case mfp of
    Just fp -> mfmSetTaggedFilePaths (toggleTagFullPath fp tagged) >> mfmCursorDown
    Nothing -> return ()

eventTick :: MFMStateM ()
eventTick = do
  chan <- use eventChan

  liftIO $ forkIO (threadDelay 1000000 >> writeBChan chan EventTick)
  eventRefreshCurrentTab

-- | copy a file from src (absolute path) to dst (directory)
-- | will warn before overwriting
-- prompt with:
-- * no
-- * cancel all
-- * overwrite
-- * overwrite all
copyFile :: FullPath -> DirectoryName -> MFMStateM ()
copyFile sfp@(sdir, sfile) ddir = do
  exists <- liftIO $ fileExist dst
  if exists then return () else do
    liftIO $ copyFileWithMetadata src dst
  where src = fullPathToFilePath sfp
        dst = fullPathToFilePath (ddir, sfile)

eventCopyTagged :: MFMStateM ()
eventCopyTagged = do
  (sdir, tags)  <- zoom stateData $ use taggedFilePaths
  ddir          <- mfmGetActiveWorkingDirectory
  if S.null tags then
    mfmSetMessage $ MessageError "No files tagged for copy"
  else do
    let sources = map (\file -> (sdir, file)) $ S.toList tags
    mapM_ (`copyFile` ddir) sources
