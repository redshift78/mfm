{- |
Module			: GitStatus
Description	: Gets the git status for files in a directory
Copyright		: (c) Fred Strauss, 2023
License			: See LICENSE file

Gets the git status, if possible, for files in a directory
-}

{-# Language OverloadedStrings #-}

module GitStatus
  ( GitStatusResult
  , GitStatusMap
  , loadGitStatus
  , gitStatusCodesForFile
  ) where

import System.Exit
import System.Process
import Data.ByteString (hGetContents)
import System.IO (Handle)
import Data.Maybe (fromJust)
import System.Posix.Files
import Control.Exception (try)
import Text.Parsec.ByteString (Parser)

import qualified Data.Text as T
import qualified Data.Map.Strict as M
import qualified Text.Parsec as P

import Types

type StatusCode   = Char
type StatusCodes  = (StatusCode, StatusCode)
type StatusLine   = (StatusCodes, FilePath)   -- ^ For parsing

type GitStatusResult  = [ (StatusCodes, FilePath) ]
type GitStatusMap     = M.Map FilePath StatusCodes

loadGitStatus :: GitStatusMap -> DirectoryName -> IO GitStatusMap
loadGitStatus map dir = do
  mroot <- gitRepositoryRoot dir
  case mroot of
    Nothing   -> return map
    Just dir  -> do
      (_, mb_stdout_hdl, _, ph) <- createProcess (proc "git" [ "status", "--porcelain" ])
        { cwd     = Just dir
        , std_out = CreatePipe
        , std_err = CreatePipe
        }

      exitcode <- waitForProcess ph
      case exitcode of
        ExitFailure code  -> return map
        ExitSuccess       -> do
                              res <- parseFH $ fromJust mb_stdout_hdl
                              case res of
                                Right res -> return $ foldr (\(scs, fp) -> M.insert (dir <> "/" <> fp) scs) map res
                                Left err  -> return map

-- | Parse a single line returned from 'git status --porcelain'
parseLine :: Parser StatusLine
parseLine = do
  sc1 <- P.anyChar
  sc2 <- P.anyChar
  P.space
  filename <- P.manyTill P.anyChar P.newline
  return ( (sc1, sc2), filename)

-- Parse many lines from 'git status --porcelain'
parseAll :: Parser [StatusLine]
parseAll = P.many parseLine

parseFH :: Handle -> IO (Either T.Text GitStatusResult)
parseFH fh = do
  contents <- hGetContents fh
  let result = P.parse parseAll "git status --porcelain" contents
  case result of
    Left err  -> return $ Left $ T.pack $ show err
    Right res -> return $ Right res

gitStatusCodesForFile :: GitStatusMap -> FilePath -> Maybe StatusCodes
gitStatusCodesForFile map fp = M.lookup fp map

-- | Get the git repository root, if any, for a given directory
gitRepositoryRoot :: DirectoryName -> IO (Maybe DirectoryName)
gitRepositoryRoot dir = do
  eexist  <- (try (fileExist gitdir) :: IO (Either IOError Bool))
  case eexist of
    Left err    -> return Nothing
    Right False -> return Nothing
    Right True  -> do
      status <- getFileStatus gitdir
      if isDirectory status then
        return $ Just dir
      else
        case parent dir of
          Just dir  -> gitRepositoryRoot dir
          Nothing   -> return Nothing
  where gitdir      = dir <> "/.git"
        parent []   = Nothing
        parent "/"  = Nothing
        parent dir  | '/' `elem` tail dir = Just $ reverse $ tail $ dropWhile (/= '/') $ reverse dir
                    | otherwise           = Nothing
