module TabLineUI
  ( uiTabLine
  ) where

import Brick.Types
import Brick.AttrMap
import Brick.Widgets.Core
import Brick.Widgets.Border

import Data.List (intersperse)
import Lens.Micro.Platform

import Types
import MFMState
import Tab

uiTabLine :: MFMState -> Widget UINames
uiTabLine state = tabTitles
  where tabTitles     = vLimit 1 $ hBox $ borderify $ zipWith applyAttr [0..] $ map (str . show) [0..9]
        borderify w   = vBorder : intersperse vBorder w ++ [ vBorder ]
        applyAttr i w | i == state^.activeTab = withAttr (attrName "attrTabActive") w
                      | otherwise             = withAttr (attrName "attrTabInactive") w
