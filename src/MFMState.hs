{-# Language OverloadedStrings #-}
{-# Language TemplateHaskell #-}

module MFMState where

import Brick.Types
import Brick.BChan (BChan, writeBChan)

import Control.Monad.IO.Class (liftIO)
import Data.Time.LocalTime (TimeZone, getCurrentTimeZone)
import Lens.Micro.Platform

import qualified Data.Text as T
import qualified Data.Map.Strict as M
import qualified Brick.Keybindings as K

import Types
import MFMEvents
import Tab
import PromptLine
import DirectoryStatus
import Pane
import PaneFileState
import GitStatus
import StateData
import Config
import Jobs

data MFMMode  = MFMNormal | MFMPromptLine deriving (Eq, Show)

data MFMState = MFMState
  { _tabs           :: [Tab]
  , _activeTab      :: Int
  , _promptLine     :: PromptLine
  , _eventChan      :: BChan MFMEvent
  , _timezone       :: TimeZone
  , _mfmMode        :: MFMMode
  , _stateData      :: StateData
  , _dispatcher     :: K.KeyDispatcher MFMEvent (EventM UINames MFMState)
  , _mfmConfig      :: Config
  }
$(makeLenses ''MFMState)

type MFMStateM a = EventM UINames MFMState a

newMFMState :: BChan MFMEvent -> K.KeyDispatcher MFMEvent (EventM UINames MFMState) -> IO MFMState
newMFMState chan dispatcher = do
  tab       <- defaultTab
  timezone  <- getCurrentTimeZone
  config    <- loadConfig
  return $ MFMState
    { _tabs       = [ tab ]
    , _activeTab  = 0
    , _promptLine = newPromptLine
    , _eventChan  = chan
    , _timezone   = timezone
    , _mfmMode    = MFMNormal
    , _stateData  = mempty
    , _dispatcher = dispatcher
    , _mfmConfig  = config
    }

{-
nextTab :: MFMState -> MFMState
nextTab state
  | state^.activetab + 1 == length (state^.tabs)  = state & activeTab .~ 0 -- state { activeTab = 0 }
  | otherwise                                     = state & activeTabe %~ (+1) -- state { activeTab = tabi + 1 }
-}

-- | Set the prompt line input
mfmSetInput :: T.Text -> MFMStateM ()
mfmSetInput input = zoom promptLine $ promptText .= input

-- | Set the state mode
mfmSetMode :: MFMMode -> MFMStateM ()
mfmSetMode mode = mfmMode .= mode

-- | Queue an event
mfmQueueEvent :: MFMEvent -> MFMStateM ()
mfmQueueEvent event = do
  chan <- use eventChan
  liftIO $ writeBChan chan event

-- | Set the prompt line message
mfmSetMessage :: PromptMessage -> MFMStateM ()
mfmSetMessage msg = zoom promptLine $ promptMessage .= msg

-- | Apply given function to the currently active tab
mfmModifyActiveTab :: (Tab -> Tab) -> MFMStateM ()
mfmModifyActiveTab f = do
  tabi      <- use activeTab
  currtabs  <- use tabs
  let tabs' = zipWith (\i t -> if i == tabi then f t else t) [0..] currtabs
  tabs .= tabs'

-- | Get the full path of the file under the cursor on the active pane
mfmGetActiveCursorFullPath :: MFMStateM (Maybe FullPath)
mfmGetActiveCursorFullPath = do
  tabs    <- use tabs
  tabi    <- use activeTab
  dirmap  <- zoom stateData $ use directoryStatusMap

  let tab = tabs !! tabi
  let (left, right) = tab ^. panes

  return $ paneCursorFullPath dirmap $ tabActivePane tab ^. fileState

-- | Get the working directory of the active pane
mfmGetActiveWorkingDirectory :: MFMStateM DirectoryName
mfmGetActiveWorkingDirectory = do
  tabs  <- use tabs
  tabi  <- use activeTab

  let tab = tabs !! tabi

  return $ tabActivePane tab ^. fileState . workingDirectory

mfmSetAvailableSpaceMap :: AvailableSpaceMap -> MFMStateM ()
mfmSetAvailableSpaceMap spacemap = zoom stateData $ availableSpaceMap .= spacemap

mfmSetDirectoryStatusMap :: DirectoryStatusMap -> MFMStateM ()
mfmSetDirectoryStatusMap dirmap = zoom stateData $ directoryStatusMap .= dirmap

mfmSetGitStatusMap :: GitStatusMap -> MFMStateM ()
mfmSetGitStatusMap gitmap = zoom stateData $ gitStatusMap .= gitmap

mfmSetTaggedFilePaths :: TaggedFilePaths -> MFMStateM ()
mfmSetTaggedFilePaths tagged = zoom stateData $ taggedFilePaths .= tagged

mfmCursorHelper :: (DirectoryStatusMap -> Tab -> Tab) -> MFMStateM ()
mfmCursorHelper f = use stateData >>= mfmModifyActiveTab . f . _directoryStatusMap

-- | mfmCursor<DIRECTION> :: MFMStateM ()
mfmCursorUp     = mfmCursorHelper moveCursorUp
mfmCursorDown   = mfmCursorHelper moveCursorDown
mfmCursorLeft   = mfmCursorHelper moveCursorLeft >> mfmQueueEvent EventRefreshCurrentTab
mfmCursorRight  = mfmCursorHelper moveCursorRight >> mfmQueueEvent EventRefreshCurrentTab

mfmOnActivePane :: (Pane -> Pane) -> MFMStateM ()
mfmOnActivePane f = do
  tabs  <- use tabs
  tabi  <- use activeTab
  let tab = tabs !! tabi
  let tab' = onActivePane f tab
  modify (\s -> s { _tabs = take tabi tabs <> [tab'] <> drop (tabi+1) tabs })

mfmSetActivePaneMode :: PaneMode -> MFMStateM ()
mfmSetActivePaneMode mode = mfmOnActivePane (setPaneMode mode)

mfmAddJob :: Job -> MFMStateM ()
mfmAddJob job = do
  jid   <- mfmNextJobID
  chan  <- use eventChan
  zoom stateData $ jobs %= M.insert jid job
  liftIO $ jobThread chan jid job
  return ()

mfmNextJobID :: MFMStateM JobID
mfmNextJobID = zoom stateData $ nextJobID <%= (+1)

mfmModifyJobAction :: JobID -> Int -> (Action -> Action) -> MFMStateM ()
mfmModifyJobAction jid ai f = zoom stateData $ jobs %= M.adjust f' jid
  where
    f' :: Job -> Job
    f' job      = job&jobActions %~ f''

    f'' :: [Action] -> [Action]
    f'' actions = (\(i, action) -> if i == ai then f action else action) <$> zip [0..] actions
