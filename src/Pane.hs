{-# Language OverloadedStrings #-}
{-# Language TemplateHaskell #-}

module Pane where

{-
 - A pane takes up half of the window, it is a single view
 - and probably either on the left or right. Panes are contained
 - in tabs, so that multiple working environments can exist.
 -}

import Jobs
import Types
import PaneFileState
import PaneJobState
import StateData
import DirectoryStatus

import Brick.Types

import Lens.Micro.Platform

data PaneMode       = PaneModeFiles | PaneModeJobs deriving (Eq, Show)
data PaneSortMode   = SortDefault deriving (Eq, Show)

data Pane = Pane
  { _mode       :: PaneMode
  , _fileState  :: PaneFileState
  , _jobState   :: PaneJobState
  }

$(makeLenses ''Pane)

defaultPane :: IO Pane
defaultPane = do
  pfstate <- defaultPaneFileState
  return $ Pane
    { _mode       = PaneModeFiles
    , _fileState  = pfstate
    , _jobState   = defaultPaneJobState
    }

-- | Apply function to appropriate state depending on pane mode
-- | fsf : PaneFileState function
-- | jsf : PaneJobState function
onPaneModeState :: (PaneFileState -> PaneFileState) -> (PaneJobState -> PaneJobState) -> Pane -> Pane
onPaneModeState fsf jsf pane = case pane ^. mode of
  PaneModeFiles -> pane & fileState %~ fsf
  PaneModeJobs  -> pane & jobState %~ jsf

paneToggleHidden :: Pane -> Pane
paneToggleHidden = onPaneModeState (over showHidden not) id

paneMoveCursorDown :: DirectoryStatusMap -> Pane -> Pane
paneMoveCursorDown dirmap = onPaneModeState (paneFileStateCursorDown dirmap) id

paneMoveCursorUp :: DirectoryStatusMap -> Pane -> Pane
paneMoveCursorUp dirmap = onPaneModeState (paneFileStateCursorUp dirmap) id

paneMoveCursorLeft :: Pane -> Pane
paneMoveCursorLeft = onPaneModeState paneFileStateCursorLeft id

paneMoveCursorRight :: DirectoryStatusMap -> Pane -> Pane
paneMoveCursorRight dirmap = onPaneModeState (paneFileStateCursorRight dirmap) id

setPaneMode :: PaneMode -> Pane -> Pane
setPaneMode nmode pane = pane & mode .~ nmode

drawPane :: StateData -> Bool -> Pane -> Widget UINames
drawPane sdata active pane
  | mode' == PaneModeFiles  = drawPaneFiles sdata active $ pane^.fileState
  | mode' == PaneModeJobs   = drawPaneJobs active jobs' $ pane^.jobState
  where mode' = pane^.mode
        jobs' = sdata^.jobs

drawPaneTitle :: Bool -> Pane -> Widget UINames
drawPaneTitle active pane
  | mode' == PaneModeFiles  = drawPaneFilesTitle active $ pane^.fileState
  | mode' == PaneModeJobs   = drawPaneJobsTitle active $ pane^.jobState
  where mode' = pane^.mode
