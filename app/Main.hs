module Main where

import AppUI
import MFMState
import MFMEvents
import MFMEventHandlers

import Brick.Main
import Brick.BChan (newBChan, writeBChan)
import Graphics.Vty (mkVty, defaultConfig)
import Brick.Keybindings (newKeyConfig, keyDispatcher)

main :: IO ()
main = do
  let buildVty  = mkVty defaultConfig
  let keyconfig = newKeyConfig allKeyEvents defaultBindings []

  eventChan   <- newBChan 100
  initialVty  <- buildVty
  dispatcher  <- case keyDispatcher keyconfig mfmEventHandlers of
    Right dispatcher  -> return dispatcher
    Left collissions  -> error "Collissions in keyDispatcher"

  uiState     <- newMFMState eventChan dispatcher

  customMain initialVty buildVty (Just eventChan) defaultApp uiState
  return ()
