* Each job has multiple actions
* Actions in a job happen sequentially
* Jobs can happen concurrently
* Each job is a thread

# Job types
* Copy files to destination (1 + 2 + 3)
* Move files to destination (1 + 2 + 3 + 4)
* Test job

# Action primatives
1. Get file size
2. Copy file to destination
3. Verify source file against destination file
4. Delete source file

# Thread communication
**jobThread** is forked. Main app keeps track of job and action statuses.
Statuses are updated via event calls from **jobThread**.
Each job has a unique **jobID**, actions are identified by their index in the
list of actions.

Main app should not quit if any jobs are still busy.

# Flow of TestJob
* **EventAddTestJob** is received
* This forks (with forkIO) a **jobHandler**

## jobHandler
* Trigger event that job is started (?)
* Get first action in queue
  * Trigger event for action progress
  * Perform step in action
  * Repeat until done
  * Trigger event for action completed
* Repeat until all actions are done
* Trigger event for job done (?)
