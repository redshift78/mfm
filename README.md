Mr. File Manager (We just say File Manager)

 ----|Tab|Tab|-------------------
| Pane          | Pane           |
|               |                |
|               |                |
|--------------------------------|
| Status line                    |
| Prompt line                    |

# Application
* 1 or more Tabs
* Status line
* Prompt line

# Tab
* 2 Panes

# Pane
* List of files

# Status line
* Shows status of currently highlighted file

# Prompt line
* Where :commands are typed
* Expandable to half screen shell
