* Ability to zoom a pane to 'fullscreen'
* Yank and Paste file
* Create new tabs
* Switch between tabs
* Remember cursors for sub dirs
* Consider identifying files with linux 'file' command
* Ability to sort by modified time
* Execute arbitrary shell commands, perhaps with :! like vim
* Consider checking list permission for directory before entering
* Ability to set marks and go to them (and save to file)
* Check for tmux using $TMUX environment variable before splitting
  window for a shell
* Get index of cursor, if file disappears, try to set the new cursor
  at the same index
* Bulk renaming like vifm
* Have one pane show history of messages, for debugging
* PgUp and PgDwn support
* Consider using System.FSNotify instead of 1s interval refreshing
* Copies should be verified
* Ability to name tabs
* Nerd font support
* Nerd font icons for job status
* Show number and size of tagged files
* Themeing
* Mark files in queue for copying or moving as busy
* Consider a generic list view pane, that can have sub items and handles cursor etc

# Job queue
* Implement cursor movement for jobs
* Queue multiple jobs
* Each job can have multiple actions
* Actions happen sequentially
* Jobs can happen concurrently, with a settable limit ?
* Any dialogs, like choices to overwrite all files, are per job
* A way to view jobs and perhaps cancel them
* EventQuit should warn if there are jobs in the queue

# Bugs
* crash if working dir is deleted from elsewhere
* git status doesn't show for directories with no files tracked by git
* Tagging works in jobs view

# Make MFM useful while in progress
* Copy files
* Delete files
* Move files
* Bulk rename files
